@extends('layouts.app')

@section('content')
<div class="container-fluid" >
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body" style="background-color:lavender;">
                    <img src="/uploads/avatars/{{Auth::User()->avatar}}" style="width:250px; height:160px; float:left;" alt="image"><!-- <a href="/editProfile"></a> --></img>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-body" style="background-color:lavender;text-align:center;">
                <h3>{{Auth::User()->name}}<h3>
                <h3>{{Auth::User()->email}}<h3>
                <h3>{{Auth::User()->mobileNo}}</h3>                    
                </div>
            </div>
        </div>        
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body" style="background-color:lavender;">
                <ul class="nav nav-pills nav-stacked">
                <li role="presentation" class="active"><a href="#">Dashboard</a></li>
                <li role="presentation"><a href="#" id="alluser">Available Food Items</a></li>
                </ul>
<!--                 <a href="/editProfile" class="btn btn-info">edit profile</a> -->
                <a href="/additem" class="btn btn-primary">addItem</a> 
                </div>      
            </div>
        </div>
        <div class="col-md-9">
            <div id="list" class="panel panel-default">
                <div class="panel-body" style="background-color:lavender;">
                <h2>All food item</h2>                    
                <table class="table table-striped">
                    <thead>
                      <tr><th>Image</th><th>Name</th><th>Price</th><th>Type</th> <th>Update</th><th>Remove</th></tr>
                    </thead>
                    <tbody>
                        @foreach (DB::table('foods')->where('restaurantid',Auth::User()->id)->get() as $food)
                        <tr>
                            <td><img src="/uploads/avatars/{{$food->foodimage}}"style="width:30px; height:30px; float:left; border-radius:50%;" alt="image"></td>
                            <td>{{ $food->foodname }}</td>
                            <td>{{ $food->foodprice }}</td>
                            <td>{{ $food->foodtype }}</td>
                            <td><i class="fa fa-plus-square"></i><a href="/updateitem/{{$food->foodid}}">update</a></td>
                            <td><i class="fa fa-trash"></i><a href="/deleteitem/{{$food->foodid}}">Remove</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

    </div>
</div>
@endsection
@section('script')
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="/js/jquery-3.2.1.min.js"></script>
<script> 
$(document).ready(function(){
    $("#alluser").click(function(){
        $("#list").slideToggle("slow");
    });
});
</script>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">add Item</div>
                <div class="panel-body">
                 <img src="/uploads/avatars/{{'default.jpg'}}" style="width:150px; height:150px; float:left; border-radius:50%; "alt="image"></img>
                <form enctype="multipart/form-data" action="/foodimageupdate" method="post">
                {{ csrf_field() }}
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Update food image</label>
                      <div class="col-sm-10">
                         <input type="file" name="foodimage">
                      </div>
                  </div>

<!--                  <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
<!--                  <input type="submit" class="pull-right btn btn-sm btn-primary">
                </form>
                </div>
                <div class="panel panel-default">
                    <form class="form-horizontal" method="post" action="/addfood">
                                  {{ csrf_field() }} -->
                      <div class="form-group">
<!--                         <label class="col-sm-2 control-label">Food Name</label> -->        
                        <div class="col-sm-10">
                        <br>
                          <input type="text" class="form-control" name="foodname"
                          placeholder="foodname">
                        </div>
                      </div>
                      <div class="form-group">
<!--                         <label class="col-sm-2 control-label">Food Price</label> -->
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="foodprice"
                          placeholder="Food Price">
                        </div>
                      </div>       
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="pull-right btn btn-sm btn-primary">Save</button>
                        </div>
                      </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit profile</div>
                <div class="panel-body">
                 <img src="/uploads/avatars/{{Auth::User()->avatar}}" style="width:150px; height:150px; float:left; border-radius:50%; "alt="image"></img>

                <form enctype="multipart/form-data" action="/imageupdate" method="post">
                {{ csrf_field() }}
                 <label>Update profile image</label>
                 <input type="file" name="avatar">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="submit" class="pull-right btn btn-sm btn-primary">
                </form>
                </div>
                <div class="panel panel-default">
                    <form class="form-horizontal" method="post" action="profileupdate">
                                  {{ csrf_field() }}
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" name="email"
                          value="{{Auth::User()->email}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="name"
                          value="{{Auth::User()->name}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">mobile No</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="mobileno"
                          value="{{Auth::User()->mobileNo}}">
                        </div>
                      </div>                      
<!--                       <div class="form-group">
                        <label class="col-sm-2 control-label">mobile No</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="col-sm-2 control-label">about me</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control">
                        </div>
                      </div>  -->                                          
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-default">Save</button>
                        </div>
                      </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>OrderByYou</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
          header{
                height:80px;
                padding-left: 30px;
                text-decoration-color: green;
                background-color:orange ;
          }
         section{
                    text-align: center;
          }
          footer {
                    padding: 10px;
                    color: white;
                    background-color:black;
                    clear: left;
                    text-align: center;
                    position: relative;
                    bottom: 0px;
                    left: 0px;
                    right:0px;
                    height:50px;
                    opacity:1;
          }
          .thumbnail img{
                    width:200px;
                    height:150px;
          }
          .bg-2{
                    text-align: center;
          }
          .bg-1{
              border-collapse: collapse;
              text-align: center;
          }
          th {
              /*                background-color: #4CAF50;*/
                background-color: orange;
                color: white;
                text-align: center;
          }
          td {
                text-align: center;
                padding: 30px 40px 20px 40px;
          }  
          .table{
              width:100%;
              height:auto;
          }  
          h2,h3,li{
            color:green;
          }        
      </style>

    </head>
    <body>
        <header>
            <div>
                <h1 style="font-size:40px;color:red;"><i class="fa fa-firefox" style="font-size:40px;color:green;"></i>OrderByMe</h1>
            </div>
        </header>
        <div class="container-fluid bg-1">
            <div class="row">
              <h2>Order delicious food online</h2><br>
              <h2>order food online from the best restaurant near you</h2>
              <div class="col-md-2">
                <div id="list" class="panel panel-default">
                  <div class="panel-body" style="">
                    <h3>food category</h3>
                    <ul>
                      @foreach($foods as $food) 
                          <li><a href="/category_list/{{$food->restaurantid}}/{{$food->foodcategories}}">{{$food->foodcategories}}</a></li>
                      
                          <li><a href="/type_list/{{$food->restaurantid}}/{{$food->foodtype}}">{{$food->foodtype}}</a></li>
                      
                      @endforeach                    
                      <!-- <li><a href="/type_list/{{$foods[0]->restaurantid}}/{{'veg'}}">Veg</a></li>
                      <li><a href="/type_list/{{$foods[0]->restaurantid}}/{{'nonveg'}}">Non-Veg</a></li>
                      <li><a href="/category_list/{{$foods[0]->restaurantid}}/{{'southindian'}}">South Indian</a></li>
                      <li><a href="/category_list/{{$foods[0]->restaurantid}}/{{'northindian'}}">North Indian</a></li>
                      <li><a href="/category_list/{{$foods[0]->restaurantid}}/{{'fastfood'}}">Fast Food</a></li>
                      <li><a href="/category_list/{{$foods[0]->restaurantid}}/{{'maincourse'}}">Main Course</a></li>  -->
                    </ul>                      
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div id="list" class="panel panel-default">
                  <div class="panel-body" style="">
                      <h2 style="color:green;">All food item</h2>   
                      <table class="table table-striped">
                        <thead>
                           <tr><th></th><th>Name</th><th>Price</th><th>Type</th> <th>add to cart</th><th>remove from cart</th></tr>
                        </thead>
                        <tbody>
                          @if(count($foods))                 
                          @foreach($foods as $food)
                            <tr>
                              <td><img src="/uploads/avatars/{{$food->foodimage}}"
                              style="width:50px; height:40px; float:left; border-radius:50%;" alt="image"></td>
                              <td style="display: none;" id="foodid">{{ $food->foodid}}</td>
                              <td>{{ $food->foodname }}</td>
                              <td>{{ $food->foodprice }}</td>
                              <td>{{ $food->foodtype }}</td>
                              <td><input type="button" value="addcart" onclick="addToCart({{$food->foodid}})"></td>
                              <td><button>remove from cart</button></td>
                            </tr>
                          @endforeach
                          @else
                            <tr>
                            <td>
                              <h2>no records available</h2>
                            </td>
                            </tr>
                          @endif
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div id="list" class="panel panel-default">
                  <div class="panel-body" style="">
                        <h3>your order</h3>

                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="container bg-2">
            <h3 style="color:green;">popular this month</h3>            
            <div class="row">
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUBtD-evtbCzXZSbK3qZ0eDOfUKzJ1MopZLjEmUYWktPe22os3" alt="Lights" style="width:100%">
                          <div class="caption">
                            <p>chineese</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2QqhksuBoYeRbE9IUXqEEUX8rbV41RkiHYFriUQ-QKYEzPuwf2A" alt="Nature" style="width:100%">
                          <div class="caption">
                            <p>Biryani</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1hlP7kwdNrqs3FzHGkH4VhnYCjPBPka6vO_OvwopaPwGTG5VC" style="width:100%">
                          <div class="caption">
                            <p>fast food</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7uJJNMG5JkPR6CM2sWJ6uH7gk0MzSPYN9wE3HWe4wLUUMdf7w" alt="Lights" style="width:100%">
                          <div class="caption">
                            <p>north indian</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1hlP7kwdNrqs3FzHGkH4VhnYCjPBPka6vO_OvwopaPwGTG5VC" alt="Nature" style="width:100%">
                          <div class="caption">
                            <p>south Indian</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUBtD-evtbCzXZSbK3qZ0eDOfUKzJ1MopZLjEmUYWktPe22os3" alt="Fjords" style="width:100%">
                          <div class="caption">
                            <p>veg food</p>
                          </div>
                        </a>
                      </div>
                    </div>
            </div>   
        </div>     
        <footer>Copyright &copy; orderbyme.com
        </footer>

        <script>
          // $(document).ready(function(){

              function addToCart($id){

                   // alert($id);

                  var i = $id;

                   $.ajax({
                            'type': 'get',
                            'url':'/additem/'+i ,
                          }).done(function(response) {
                                alert(response);
                            // error: function(xhr) {
                            //    alert("error");
                            // }
                  }); 
              }

          // });
        </script>        
    </body>
</html>

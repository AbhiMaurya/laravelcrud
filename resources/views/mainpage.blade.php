<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>OrderByYou</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            body{

            }
            header{
                background-color: transparent;
                height:80px;
                padding-left: 30px;
                text-decoration-color: green;
                background-color:orange ;
            }
            section{
                    text-align: center;
            }
            footer {
                    padding: 10px;
                    color: white;
                    background-color:black;
                    clear: left;
                    text-align: center;
                    position: relative;
                    bottom: 0px;
                    left: 0px;
                    right:0px;
                    height:50px;
                    opacity:1;
            }
            .thumbnail img{
                    width:200px;
                    height:150px;
            }
            .bg-1{
                    background-image: url("https://asia-public.foodpanda.com/assets/production/in/layout/themes/capricciosa_foodpanda/images/en/homepage-splash.jpg?v=1459438060");
                    font-family: sans-serif;
                    text-align: center;
                    height: 400px;
                    background-size: cover;
                    background-position: center;
                    box-shadow: inset 0 6px 8px -6px rgba(0,0,0,.1);
                    position: relative; 
                    z-index:1 ;         
            }
            .overlay{
                    position: absolute;
                    top: 0;
                    left: 0;
                    z-index: 1;
                    width: 100%;
                    height: 100%;
                     background: -webkit-gradient(linear,left top,left bottom,color-stop(0,#fff),color-stop(10%,#fff),color-stop(40%,rgba(255,255,255,.75)),color-stop(100%,rgba(255,255,255,.4))); 
                     background: -webkit-linear-gradient(270deg,#fff 0,#fff 10%,rgba(255,255,255,.75) 40%,rgba(255,255,255,.4) 100%); 
                     background: -webkit-linear-gradient(top,#fff 0,#fff 10%,rgba(255,255,255,.75) 40%,rgba(255,255,255,.4) 100%); 
                     background: linear-gradient(180deg,#fff 0,#fff 10%,rgba(255,255,255,.75) 40%,rgba(255,255,255,.4) 100%); 
                    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#FFFFFF', GradientType=0);
            }
            .bg-2{
                    text-align: center;
            }
            .bg-0{
                    font-family: sans-serif;
                    text-align: center;
                    background-size: cover;
                    background-position: center;
                    box-shadow: inset 0 6px 8px -6px rgba(0,0,0,.1);
                    position: relative; 
                    z-index:1 ;         
            }            
            .search_box{
                border: 0;
                background: rgba(255,255,255,.5);
                padding: 30px 40px 20px 40px;
                margin: 20px auto;
                border-radius: 4px;
                color: #444;
                max-width: 960px;
            }
            th {
                background-color: orange;
                color: white;
                text-align: center;
            }
             td {
                text-align: center;
                padding: 30px 40px 20px 40px;
            }
            .table{
              margin-left: 250px;
              width:60%;
              height:auto;
            }  
        </style>
    </head>
    <body>
        <header>
            <div>
                <h1 class="fa fa-firefox" style="font-size:40px;color:green;">OrderByMe</h1>
            </div>
        </header>
        <div class="container-fluid bg-1">
            <div class="row">
                <h2 style="color:green;">Order delicious food online</h2><br>
                <h2>order food online from the best restaurant near you</h2>
                  <div class="search_box">
                    <form action="/search" method="POST" class="form-inline">
                        {{ csrf_field() }}
                        <input type="text" class="form-control" name="q"
                                    placeholder="search city" style="">
                        <input type="text" class="form-control" style="width:200px;" name="r" placeholder="Search Restaurant" style="">
                        <input class="btn btn-success"type="submit">
                    </form>
                  </div>
            </div>
<!--             <div class="overlay">
                
            </div> -->
        </div>
        <div class="container bg-0">
           <div class="row">
                @if(isset($details))
                    <h2>Restaurants available in <b> {{ $query }}</b>:</h2>
                    <table class="table table-striped table table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>rating</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($details as $user)
                            <tr>
                                <td><a href="/food_list/{{$user->id}}"><img src="/uploads/avatars/{{$user->avatar}}"style="width:100px; height:100px; float:left;" 
                                alt="image"></a></td>
                                <td><a href="/food_list/{{$user->id}}">{{$user->name}}</a></td>
                                <td><a href="/food_list/{{$user->id}}">{{$user->rating}}</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
           </div> 
        </div>
        <div class="container-fluid bg-2">
            <h3>popular this month</h3>            
            <div class="row">
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUBtD-evtbCzXZSbK3qZ0eDOfUKzJ1MopZLjEmUYWktPe22os3" alt="Lights" style="width:100%">
                          <div class="caption">
                            <p>chineese</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2QqhksuBoYeRbE9IUXqEEUX8rbV41RkiHYFriUQ-QKYEzPuwf2A" alt="Nature" style="width:100%">
                          <div class="caption">
                            <p>Biryani</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1hlP7kwdNrqs3FzHGkH4VhnYCjPBPka6vO_OvwopaPwGTG5VC" style="width:100%">
                          <div class="caption">
                            <p>fast food</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7uJJNMG5JkPR6CM2sWJ6uH7gk0MzSPYN9wE3HWe4wLUUMdf7w" alt="Lights" style="width:100%">
                          <div class="caption">
                            <p>north indian</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1hlP7kwdNrqs3FzHGkH4VhnYCjPBPka6vO_OvwopaPwGTG5VC" alt="Nature" style="width:100%">
                          <div class="caption">
                            <p>south Indian</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="thumbnail">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUBtD-evtbCzXZSbK3qZ0eDOfUKzJ1MopZLjEmUYWktPe22os3" alt="Fjords" style="width:100%">
                          <div class="caption">
                            <p>veg food</p>
                          </div>
                        </a>
                      </div>
                    </div>
            </div>   
        </div>     
        <footer>Copyright &copy; OrderByMe.Com
        </footer>
    </body>
</html>

@extends('layouts.app')

@section('content')
<div class="container-fluid" >
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body" style="background-color:lavender;">
                    <img src="/uploads/avatars/{{Auth::User()->avatar}}" style="width:250px; height:160px; float:left;" alt="image">                  
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-body" style="background-color:lavender; text-align:center;">
                    <h3>{{Auth::User()->name}}<h3>
                    <h3>{{Auth::User()->email}}<h3>
                    <h3>{{Auth::User()->mobileNo}}</h3>                   
                </div>
            </div>    
        </div>        
    </div>
<!-- </div>
<div class="container-fluid"> -->
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
            <div class="panel-body" style="background-color:lavender;">
                <ul class="nav nav-pills nav-stacked">
                <li role="presentation" class="active"><a href="#" id="allrestaurant">All Restaurant</a></li>
                <li role="presentation"><a href="#" id="alluser">All users</a></li>  </ul>
            </div>
            </div>
        </div>
        <div class="col-md-9">
            <div id="list" class="panel panel-default">
               <div class="panel-body" style="background-color:lavender;">
                <h2>All Restaurant</h2>                    
                <table class="table table-striped">
                    <thead>
                      <tr><th>Image</th><th>Name</th><th>Email</th><th>mobile No</th><th>address</th><th>rating</th></tr>
                    </thead>
                    <tbody>
                        @foreach (DB::table('users')->where('role',"1")->get() as $user)
                        <tr>
                            <td><img src="/uploads/avatars/{{$user->avatar}}"style="width:30px; height:30px; float:left; border-radius:50%;" alt="image"></td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>9876543210</td>
                            <td>{{$user->address}}</td>
                            <td>{{$user->rating}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
               </div>
            </div>
            <div id="userlist" class="panel panel-default">
               <div class="panel-body" style="background-color:lavender;">
                <h2>All user</h2>                    
                <table class="table table-striped">
                    <thead>
                      <tr><th>Image</th><th>Name</th><th>Email</th><th>mobile No</th><th>address</th><th>rating</th></tr>
                    </thead>
                    <tbody>
                        @foreach (DB::table('users')->where('role',"1")->get() as $user)
                        <tr>
                            <td><img src="/uploads/avatars/{{$user->avatar}}"style="width:30px; height:30px; float:left; border-radius:50%;" alt="image"></td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>9876543210</td>
                            <td>{{$user->address}}</td>
                            <td>{{$user->rating}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
               </div> 
            </div>
        </div>
    </div>
</div>

    </div>
</div>
@endsection
@section('script')
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="/js/jquery-3.2.1.min.js"></script>
<script> 
$(document).ready(function(){
    $("#allrestaurant").click(function(){
        $("#list").slideToggle("slow");
    });
    $("#alluser").click(function(){
        $("#userlist").slideToggle("slow");
    });
});
</script>
@endsection
<?php

namespace App\Http\Controllers;
use App\Food;
use App\Cart;
use App\CartItem;
use App\User;
use Auth;
use Image;
use DB;
use Input;


use Illuminate\Http\Request;

class CartController extends Controller
{
  /* public function __construct(){

   			$this->middleware('Auth');
   }*/

   public function addItem($productId){

   		dd($productId);
	   		
		$cart = Cart::where('user_id',Auth::user()->id)->first();
 
        if(!$cart){
            $cart =  new Cart();
            $cart->user_id=Auth::user()->id;
            $cart->save();
        }
 
        $cartItem  = new Cartitem();
        $cartItem->product_id=$productId;
        $cartItem->cart_id= $cart->id;
        $cartItem->save();

        echo json_encode($cartItem); 
        
        // return $cartObj;
 
        // return redirect('/cart');
   }

   public function showCart(){

		$cart = Cart::where('user_id',Auth::user()->id)->first();
 
        if(!$cart){
            $cart =  new Cart();
            $cart->user_id=Auth::user()->id;
            $cart->save();
        }
 
        $items = $cart->cartItems;
        $total=0;
        foreach($items as $item){
            $total+=$item->product->price;
        }
 
        return view('cart.view',['items'=>$items,'total'=>$total]);
   }
   
    public function removeItem($id){
 
        CartItem::destroy($id);
        return redirect('/cart');
    }

}

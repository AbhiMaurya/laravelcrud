<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use App\Food;
use DB;
use Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function update_avatar(Request $request)
    {
        if($request->hasFile('avatar')){
          $avatar   = $request->file('avatar');
          $filename = time().'.'.$avatar->getClientOriginalExtension();
           Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/'.$filename)); 

            $user = Auth::User();
            $user->avatar = $filename;
            $user->save();
        }

         return redirect('home');
    }
    public function update_profile(Request $request)
    {
        $user = Auth::User();
        $user->name     = $request->input('name');
        $user->email    = $request->input('email');
        $user->mobileNo = $request->input('mobileno');
        $user->save();
        
        return redirect('home');
    }
    public function update_foodimage(Request $request)
    {
        if($request->hasFile('foodimage')){
          $foodimage   = $request->file('foodimage');
          $filename = time().'.'.$foodimage->getClientOriginalExtension();
           Image::make($foodimage)->resize(300,300)->save( public_path('/uploads/avatars/'.$filename)); 

            $food = new Food;
            $food->foodimage    = $filename;
            $food->foodname     = $request->input('foodname');
            $food->foodprice    = $request->input('foodprice');
            $food->restaurantid = Auth ::User()->id;
            $food->save();
        }

         return redirect('home');
    }

    public function addfood(Request $request)
    {
        $food = new Food;

        $food->foodname     = $request->input('foodname');
        $food->foodprice    = $request->input('foodprice');
        $food->restaurantid = Auth ::User()->id;
        $food->save();
        
        return redirect('home');
    }
    public function deleteitem(Request $request){
            $id= $request->id ;

            DB::table('foods')->where('foodid', $id)->delete();

            return redirect('home');     
    }
    public function update_fooditem($id)
    {
        $fooditem = DB::table('foods')->where('foodid', $id)->first(); 
        return view('updateitem')->with('food', $fooditem);
    }
    public function update_foodrecord($id,Request $request)
    {
        $foodname     = $request->foodname ;
        $foodprice    = $request->foodprice ;
        $foodtype     = $request->foodtype;
        $foodcategory = $request->foodcategories ;
        if($request->hasFile('foodimage')){
          $foodimage   = $request->file('foodimage');
          $filename = time().'.'.$foodimage->getClientOriginalExtension();
           Image::make($foodimage)->resize(300,300)->save( public_path('/uploads/avatars/'.$filename)); 

          DB::table('foods')->where('foodid', $id)
                            ->update(['foodname' => $foodname, 'foodprice' =>$foodprice, 'foodimage'=>$filename, 'foodtype'=>$foodtype ,'foodcategories' =>$foodcategory ]);
        }
        else{
          DB::table('foods')->where('foodid', $id)
                            ->update(['foodname' => $foodname, 'foodprice' =>$foodprice,'foodtype'=>$foodtype ,'foodcategories' =>$foodcategory ]);
        }

        return view('home');
    }

    public function search_restaurant(Request $request)
    { 
        $q = Input::get ('q');
        $r = Input::get('r');
        $user = DB::table('users')->where('address', $q)
                                  ->where('role','!=',"2")
                                  ->orwhere('name', $r)
                                  ->get();
        if(count($user) > 0)
          return view('mainpage')->withDetails($user)->withQuery ( $q );
        else return view ('mainpage')->withMessage('No Details found. Try to search again !');
    }
    public function food_list($id){

      // $fooditem = DB::table('foods')->where('restaurantid', $id)->get();
      $fooditem = food::where('restaurantid', $id)->get();

      return view('food_list')->with('foods', $fooditem);
    }
    
    public function type_list($id,$type){

      $fooditem = DB::table('foods')->where('restaurantid', $id)
                                    ->where('foodtype',$type)
                                    ->get();
         if($fooditem){
            return view('food_list')->with('foods', $fooditem);    
         }                           
         else{
               return view('food_list')->with('bye');
         }
    }   
    public function category_list($id,$category){

      // $fooditem = DB::table('foods')->where('restaurantid', $id)
      //                               ->where('foodcategories',$category)
      //                               ->get();
      
      $fooditem = Food::where('restaurantid', $id)
                        ->where('foodcategories',$category)
                        ->get();                       
      return view('food_list')->with('foods', $fooditem);
    }    
}

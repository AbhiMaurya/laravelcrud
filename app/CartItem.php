<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart_item extends Model
{
    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }
 
    public function food()
    {
        return $this->belongsTo('App\food');
    }
}

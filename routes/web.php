<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')
										   ->middleware('admincheck');

Route::post('imageupdate', 'HomeController@update_avatar');

Route::get('editProfile',function(){
	return view('editProfile');
});

Route::post('profileupdate', 'HomeController@update_profile');

Route::get('adminpage',function(){
	return view('admin');
});

Route::get('additem',function(){
	return view('additem');
});

Route::post('foodimageupdate','HomeController@update_foodimage');

Route::post('addfood', 'HomeController@addfood');

Route::get('deleteitem/{id}', 'HomeController@deleteitem');

Route::get('updateitem/{id}','HomeController@update_fooditem');

Route::post('updatefooditem/{id}','HomeController@update_foodrecord');

// Route::get('mainpage','HomeController@main_page');

Route::post('search','HomeController@search_restaurant');

Route::get('food_list/{id}','HomeController@food_list');

Route::get('mainpage',function(){
	return view('mainpage');
});

Route::get('type_list/{id}/{type}','HomeController@type_list');

Route::get('category_list/{id}/{category}','HomeController@category_list');

Route::get('additem/{id}','CartController@addItem');
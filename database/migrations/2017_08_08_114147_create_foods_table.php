<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('foods', function (Blueprint $table) {
            $table->increments('foodid');
            $table->integer('restaurantid')->unsigned();
            $table->foreign('restaurantid')->references('id')->on('users');
            $table->string('foodname');
            $table->string('foodprice')->default("0");
            $table->string('foodtype')->default("veg");
            $table->string('foodcategories')->default("main course");  
            $table->string('fooddescription')->default("chilli,pepper");
            $table->string('offerprice')->default("100");
            $table->string('foodimage')->default('default.jpg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                Schema::dropIfExists('foods');
    }
}
